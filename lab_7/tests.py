from django.test import TestCase, Client
from .views import index, add_friend, validate_npm, delete_friend, friend_list, paginate_page
from django.urls import resolve
from .models import Friend
from unittest.mock import patch
from django.db.models.manager import Manager
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .api_csui_helper.csui_helper import CSUIhelper

class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)
    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)
    def test_lab7_using_friend_list_func(self):
        found = resolve('/lab-7/friend-list/')
        self.assertEqual(found.func, friend_list)
    def test_get_friend_list_data_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)
    def test_friend_list_data_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)
    def test_model_can_create_new_friend(self):
        new_friend = Friend.objects.create(friend_name='Jihan', npm='1602134566')
        counting_all_object_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_object_friend,1)
    
    def test_add_friend(self):
        response_post = Client().post('/lab-7/add-friend/',{'name': 'Jihan Amalia Irfani', 'npm': '1606837846'})
        self.assertEqual(response_post.status_code, 200)
    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken':False})
    def test_delete_friend(self):
        friend = Friend.objects.create(friend_name="Hanji", npm="160600456")
        response = Client().post('/lab-7/delete-friend/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, Friend.objects.all())
    def test_wrong_password(self):
        with self.assertRaises(Exception):
            csui_helper2 = CSUIhelper(username = "siapa", password="ya")
    def test_csui_helper_param(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
    def test_invalid_page_pagination_number(self):
        data = ["jihan", "amalia", "z", "ini", "anak", "fasil", "kom", "siapa", "ya", "oooh",
        "loro", "siji", "inites", "banget", "ppw", "asik", "tes", "aja", "mana", "dari",
        "telu", "papat", "limo", "enem", "pitu", "wolu", "songo", "sepoloh", "jawa", "niye"]
        test1 = paginate_page("...", data)
        test2 = paginate_page(-1, data)
        with patch.object(Manager, 'get_or_create') as a:
            a.side_effect = PageNotAnInteger("page number is not an integer")
            res = paginate_page("...", data)
            self.assertEqual(res['page_range'], test1['page_range'])
            with patch.object(Manager, 'get_or_create') as a:
                a.side_effect = EmptyPage("page number is less than 1")
                res = paginate_page(-1, data)
                self.assertEqual(res['page_range'], test2['page_range'])
    def test_lab7_csui_helper_initiate(self):
        csui_helper = CSUIhelper()
        self.assertTrue(csui_helper.instance is not None)
