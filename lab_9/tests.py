from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import index
from .api_enterkomputer import get_drones, get_opticals, get_soundcards
from .csui_helper import get_client_id, verify_user, get_access_token, get_data_user
import requests
from .custom_auth import auth_logout, auth_login
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"

# Create your tests here.
class Lab9UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)
    def test_lab9_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)
    def test_drone_api_enterkomputer_func(self):
        drone_json = 'https://www.enterkomputer.com/api/product/drone.json'
        drones_li = requests.get(drone_json)
        self.assertEqual(get_drones().json(), drones_li.json())
    def test_client_id_func(self):
        id_klien = 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'
        self.assertEqual(get_client_id(), id_klien)
    def test_optical_api_enterkomputer_func(self):
        optical_api = 'https://www.enterkomputer.com/api/product/optical.json'
        opticals_li = requests.get(optical_api)
        self.assertEqual(get_opticals().json(), opticals_li.json())
    def test_soundcard_api_enterkomputer_func(self):
        soundcard_api= 'https://www.enterkomputer.com/api/product/soundcard.json'
        soundcards_li = requests.get(soundcard_api)
        self.assertEqual(get_soundcards().json(), soundcards_li.json())
    def test_auth_login_logout(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-9/custom_auth/logout/')
        html_response = self.client.get('/lab-9/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", html_response)
    def test_profile(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('ghina.almira', html_response)
       
    def test_logged_in(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        response = self.client.get(reverse('lab-9:index'))
        response = self.client.get(reverse('lab-9:profile'))
        self.assertEqual(response.status_code, 200)

    def test_profile_not_login(self):
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

    def test_on_cookies(self):
        user_cookie = 'ghalsaname'
        password_cookie = 'ghalsapassword'

        #not logged in
        response = self.client.post('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_9/cookie/login.html')
        response = self.client.post('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 302)

        #get method for authorization
        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)

        #invalid login
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'coba', 'password': 'ya'})
        response = self.client.get(reverse('lab-9:cookie_login'))
        html_response= response.content.decode('utf-8')
        self.assertIn("Username atau Password Salah", html_response)
        
        #valid login
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': user_cookie, 'password': password_cookie})
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('lab-9:cookie_login'))
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertTemplateUsed(response, 'lab_9/cookie/profile.html')

        #get profile without valid login
        response = self.client.get(reverse('lab-9:cookie_profile'))
        response.client.cookies['user_login'] = 'kkk'
        response = self.client.get(reverse('lab-9:cookie_profile'))
        html_response = response.content.decode('utf-8')
        self.assertIn("Kamu tidak punya akses :P ", html_response)
        
        #test on clear function
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': user_cookie, 'password': password_cookie})
        response = self.client.get('/lab-9/cookie/clear/')
        self.assertEqual(response.status_code, 302)
    def test_logout(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-9/custom_auth/logout/')
        html_response = self.client.get('/lab-9/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", html_response)
    def test_on_drones(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})        
        
        response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107894}))
        response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107893}))
        response = self.client.post(reverse('lab-9:profile'))
        
        response_post = self.client.post(reverse('lab-9:del_session_drones', kwargs={'id':107894}))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Berhasil hapus dari favorite',html_response)
        
        response_post = self.client.post(reverse('lab-9:clear_session_drones'))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Berhasil reset favorite drones',html_response)
        
        response_post = self.client.post(reverse('lab-9:auth_logout'))
    
    def test_opticals(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        
        response = self.client.post('/lab-9/add_session_item/opticals/' + get_opticals().json()[0]["id"] + '/')
        response = self.client.post('/lab-9/add_session_item/opticals/' + get_opticals().json()[1]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah opticals favorite", html_response)
        
        response = self.client.post('/lab-9/del_session_item/opticals/' + get_opticals().json()[0]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus item opticals dari favorite", html_response)
        
        response = self.client.post('/lab-9/clear_session_item/opticals/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus session : favorite opticals", html_response)
    
    def test_soundcards(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        
        response = self.client.post('/lab-9/add_session_item/soundcards/' + get_soundcards().json()[0].get("id") + '/')
        response = self.client.post('/lab-9/add_session_item/soundcards/' + get_soundcards().json()[1].get("id") + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah soundcards favorite", html_response)
        
        response = self.client.post('/lab-9/del_session_item/soundcards/' + get_soundcards().json()[0].get("id") + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus item soundcards dari favorite", html_response)
        
        response = self.client.post('/lab-9/clear_session_item/soundcards/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus session : favorite soundcards", html_response)

    def test_get_data_user_func(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        self.npm = "1606917815"
        access_token = get_access_token(self.username, self.password)
        parameters = {"access_token":access_token, "client_id":get_client_id()}
        response = requests.get(API_MAHASISWA+self.npm, params=parameters)
        result = get_data_user(access_token, self.npm)
        self.assertEqual(result, response.json())
    def test_invalid_sso(self):
        user = "ghina"
        passw = "almira"
        with self.assertRaises(Exception) as context:
            get_access_token(user, passw)
        self.assertIn("ghina", str(context.exception))
    def test_invalid_auth_login(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': 'coba', 'password': 'ya'})
        response = self.client.get(reverse('lab-9:index'))
        html_response = response.content.decode('utf-8')
        self.assertIn("Username atau password salah", html_response)
