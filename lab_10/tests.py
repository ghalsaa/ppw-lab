from django.test import TestCase
from django.test import Client


# Create your tests here.
class Lab10UnitTest(TestCase):
    def test_lab_10_url_is_exist(self):
        response = Client().get('/lab-10/')
        self.assertEqual(response.status_code, 200)
